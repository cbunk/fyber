package com.example.cbunk.myapplication.weather;

import javax.inject.Inject;

import com.example.cbunk.myapplication.weather.model.WeatherService;
import com.example.cbunk.myapplication.weather.model.pojo.ForecastDailyResponse;
import com.example.cbunk.myapplication.weather.model.pojo.Main;
import com.example.cbunk.myapplication.weather.model.pojo.WeatherResponse;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class WeatherModel implements WeatherContract.Model {

    private WeatherService weatherService;

    @Inject
    public WeatherModel(final WeatherService weatherService) {
        this.weatherService = weatherService;

    }

    @Override
    public void getCurrentWeather(final int cityId, final LoadCurrentWeatherCallback weatherCallback) {

        WeatherResponse weatherResponse = new WeatherResponse.Builder().name("Brandenburg")
                                                                       .main(new Main.Builder().temp(34d).build())
                                                                       .build();
        weatherCallback.onCurrentWeatherLoaded(weatherResponse);
    }

    @Override
    public void getWeatherForecast(final int cityId, final LoadWeatherForecastCallback loadWeatherForecastCallback) {
        weatherService.dailyForecast(cityId).enqueue(new Callback<ForecastDailyResponse>() {
                @Override
                public void onResponse(final Response<ForecastDailyResponse> response, final Retrofit retrofit) {
                    loadWeatherForecastCallback.onWeatherForecastLoaded(response.body());
                }

                @Override
                public void onFailure(final Throwable t) { }
            });
    }

}
