package com.example.cbunk.myapplication.fyber;

import com.example.cbunk.myapplication.fyber.model.pojo.Fyber;
import com.example.cbunk.myapplication.fyber.model.pojo.Offer;
import com.google.common.collect.Lists;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.List;

import rx.Observable;
import rx.Scheduler;
import rx.android.plugins.RxAndroidPlugins;
import rx.android.plugins.RxAndroidSchedulersHook;
import rx.schedulers.Schedulers;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by christian on 20.03.16.
 */
public class FyberPresenterTest {

    @Mock
    FyberContract.Model model;

    @Mock
    FyberContract.View view;

    private FyberPresenter fyberPresenter;

    @Before
    public void setup() {

        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        RxAndroidPlugins.getInstance().registerSchedulersHook(new RxAndroidSchedulersHook() {
            @Override
            public Scheduler getMainThreadScheduler() {
                return Schedulers.immediate();
            }
        });

        fyberPresenter = spy(new FyberPresenter(model));
        fyberPresenter.setView(view);
    }

    @Test
    public void testGetOffersSuccess() {
        Fyber fyber = new Fyber();
        List<Offer> offers = Lists.newArrayList();
        fyber.setOffers(offers);

        int page = 1;
        String uId = "";
        String apiKey = "";
        String appId = "";
        String pub0 = "";

        when(model.getOffers(eq(page), Mockito.anyString(), eq(appId),
                eq(uId), Mockito.anyString(), Mockito.anyString(),
                eq(pub0))).thenReturn(Observable.just(fyber));

        fyberPresenter.getOffers(page, uId, apiKey, appId, pub0);

        verify(model).getOffers(eq(page), Mockito.anyString(), eq(appId),
                eq(uId), Mockito.anyString(), Mockito.anyString(),
                eq(pub0));

        verify(view).showOffers(offers);



    }

    @Test
    public void testGetOffersError() {
        String errorMessage = "a message";
        when(model.getOffers(Mockito.anyInt(), Mockito.anyString(), Mockito.anyString(),
                Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
                Mockito.anyString())).thenReturn(Observable.<Fyber>error(new Exception(errorMessage)));

        fyberPresenter.getOffers(1, "", "", "", "");

        // Callback is captured and invoked with stubbed weatherResponse
        verify(model).getOffers(Mockito.anyInt(), Mockito.anyString(), Mockito.anyString(),
                Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
                Mockito.anyString());

        verify(view).showErrorMessage(errorMessage);
    }

    @After
    public void tearDown() {
        RxAndroidPlugins.getInstance().reset();
    }
}
