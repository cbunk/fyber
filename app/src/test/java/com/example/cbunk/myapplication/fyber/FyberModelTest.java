package com.example.cbunk.myapplication.fyber;

import com.example.cbunk.myapplication.fyber.model.FyberModel;
import com.example.cbunk.myapplication.fyber.model.FyberService;
import com.example.cbunk.myapplication.fyber.model.pojo.Fyber;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import rx.Subscriber;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

public class FyberModelTest {

    @Mock
    FyberService fyberService;

    private FyberModel fyberModel;

    @Before
    public void setup() {

        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        fyberModel = spy(new FyberModel(fyberService));
    }

    @Test
    public void testGetOffers() {

        int page = 1;
        String any = "a";

        fyberModel.getOffers(page, any, any,
                any, any, any, any);

        verify(fyberService).getOffers(eq(any), Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), eq(page), eq(any), Mockito.anyLong(), eq(any));
    }

    @Test
    public void testGetOffersWhenFormatIsNull() {

        int page = 1;
        String uId = "abs";
        String appId = "abc";
        String pub0 = "abc";

        fyberModel.getOffers(page, null, appId,
                uId, "", "", pub0).subscribe(new Subscriber<Fyber>() {
            @Override
            public void onCompleted() {
                Assert.fail("should not be called");
            }

            @Override
            public void onError(Throwable e) {
                Assert.assertEquals(e.getMessage(), "format must not be null");
            }

            @Override
            public void onNext(Fyber fyber) {
                Assert.fail("should not be called");
            }
        });
    }

    @Test
    public void testGetOffersWhenFormatIsEmpty() {

        int page = 1;
        String uId = "abs";
        String appId = "abc";
        String pub0 = "abc";

        fyberModel.getOffers(page, "", appId,
                uId, "", "", pub0).subscribe(new Subscriber<Fyber>() {
            @Override
            public void onCompleted() {
                Assert.fail("should not be called");
            }

            @Override
            public void onError(Throwable e) {
                Assert.assertEquals(e.getMessage(), "format must not be empty");
            }

            @Override
            public void onNext(Fyber fyber) {
                Assert.fail("should not be called");
            }
        });
    }

    @Test
    public void testGetOffersWhenPageIsLessThanOne() {

        fyberModel.getOffers(-1, "abc", "abc",
                "abc", "abc", "abc", "abc").subscribe(new Subscriber<Fyber>() {
            @Override
            public void onCompleted() {
                Assert.fail("should not be called");
            }

            @Override
            public void onError(Throwable e) {
                Assert.assertEquals(e.getMessage(), "page must not be less than 1");
            }

            @Override
            public void onNext(Fyber fyber) {
                Assert.fail("should not be called");
            }
        });
    }

    @Test
    public void testGetOffersWhenApiIdIsNull() {

        fyberModel.getOffers(1, "abc", null,
                "abc", "abc", "abc", "abc").subscribe(new Subscriber<Fyber>() {
            @Override
            public void onCompleted() {
                Assert.fail("should not be called");
            }

            @Override
            public void onError(Throwable e) {
                Assert.assertEquals(e.getMessage(), "appId must not be null");
            }

            @Override
            public void onNext(Fyber fyber) {
                Assert.fail("should not be called");
            }
        });
    }

    @Test
    public void testGetOffersWhenApiIdIsEmpty() {

        fyberModel.getOffers(1, "abc", "",
                "abc", "abc", "abc", "abc").subscribe(new Subscriber<Fyber>() {
            @Override
            public void onCompleted() {
                Assert.fail("should not be called");
            }

            @Override
            public void onError(Throwable e) {
                Assert.assertEquals(e.getMessage(), "appId must not be empty");
            }

            @Override
            public void onNext(Fyber fyber) {
                Assert.fail("should not be called");
            }
        });
    }

    @Test
    public void testGetOffersWhenUidIsNull() {

        fyberModel.getOffers(1, "abc", "a",
                null, "abc", "abc", "abc").subscribe(new Subscriber<Fyber>() {
            @Override
            public void onCompleted() {
                Assert.fail("should not be called");
            }

            @Override
            public void onError(Throwable e) {
                Assert.assertEquals(e.getMessage(), "uId must not be null");
            }

            @Override
            public void onNext(Fyber fyber) {
                Assert.fail("should not be called");
            }
        });
    }

    @Test
    public void testGetOffersWhenUidIsEmpty() {

        fyberModel.getOffers(1, "abc", "a",
                "", "abc", "abc", "abc").subscribe(new Subscriber<Fyber>() {
            @Override
            public void onCompleted() {
                Assert.fail("should not be called");
            }

            @Override
            public void onError(Throwable e) {
                Assert.assertEquals(e.getMessage(), "uId must not be empty");
            }

            @Override
            public void onNext(Fyber fyber) {
                Assert.fail("should not be called");
            }
        });
    }

    @Test
    public void testGetOffersWhenLocaleIsNull() {

        fyberModel.getOffers(1, "abc", "a",
                "a", null, "abc", "abc").subscribe(new Subscriber<Fyber>() {
            @Override
            public void onCompleted() {
                Assert.fail("should not be called");
            }

            @Override
            public void onError(Throwable e) {
                Assert.assertEquals(e.getMessage(), "locale must not be null");
            }

            @Override
            public void onNext(Fyber fyber) {
                Assert.fail("should not be called");
            }
        });
    }

    @Test
    public void testGetOffersWhenLocaleIsEmpty() {

        fyberModel.getOffers(1, "abc", "a",
                "a", "", "abc", "abc").subscribe(new Subscriber<Fyber>() {
            @Override
            public void onCompleted() {
                Assert.fail("should not be called");
            }

            @Override
            public void onError(Throwable e) {
                Assert.assertEquals(e.getMessage(), "locale must not be empty");
            }

            @Override
            public void onNext(Fyber fyber) {
                Assert.fail("should not be called");
            }
        });
    }


}
