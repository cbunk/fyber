package com.example.cbunk.myapplication.fyber;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.cbunk.myapplication.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasErrorText;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class FyberFormActivityTest {
    /**
     * {@link ActivityTestRule} is a JUnit {@link Rule @Rule} to launch your activity under test.
     *
     * <p>Rules are interceptors which are executed for each test method and are important building blocks of Junit
     * tests.
     */
    @Rule
    public ActivityTestRule<FyberFormActivity> fyberFormActivityTestRule = new ActivityTestRule<>(FyberFormActivity.class);

    @Test
    public void showForm() throws Exception {

        onView(withId(R.id.fyber_form_apikey_edittext)).check(matches(isDisplayed()));
        onView(withId(R.id.fyber_form_appid_edittext)).check(matches(isDisplayed()));
        onView(withId(R.id.fyber_form_pub0_edittext)).check(matches(isDisplayed()));
        onView(withId(R.id.fyber_form_submit_button)).check(matches(isDisplayed()));
        onView(withId(R.id.fyber_form_uid_edittext)).check(matches(isDisplayed()));
    }

    @Test
    public void apiKeyMustNotBeEmpty() throws Exception {

        onView(withId(R.id.fyber_form_apikey_edittext)).perform(clearText());

        onView(withId(R.id.fyber_form_submit_button)).perform(click());

        onView(withId(R.id.fyber_form_apikey_edittext)).check(matches(hasErrorText("ApiKey must not be empty")));
    }

    @Test
     public void appIdMustNotBeEmpty() throws Exception {

        onView(withId(R.id.fyber_form_appid_edittext)).perform(clearText());

        onView(withId(R.id.fyber_form_submit_button)).perform(click());

        onView(withId(R.id.fyber_form_appid_edittext)).check(matches(hasErrorText("appId must not be empty")));
    }

    @Test
    public void uIdMustNotBeEmpty() throws Exception {

        onView(withId(R.id.fyber_form_uid_edittext)).perform(clearText());

        onView(withId(R.id.fyber_form_submit_button)).perform(click());

        onView(withId(R.id.fyber_form_uid_edittext)).check(matches(hasErrorText("uid must not be empty")));
    }
}
