package com.example.cbunk.myapplication.core.dagger.application;

import com.example.cbunk.myapplication.core.BaseApplication;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by cbunk on 15.12.15.
 */
@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {
    void inject(BaseApplication application);


}
