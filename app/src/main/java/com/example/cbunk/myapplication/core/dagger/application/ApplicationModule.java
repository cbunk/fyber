package com.example.cbunk.myapplication.core.dagger.application;

import javax.inject.Singleton;

import com.example.cbunk.myapplication.core.BaseApplication;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * @author  Christian Bunk <christian.bunk@zalando.de>
 */
@Module
public class ApplicationModule {
    private final BaseApplication application;

    public ApplicationModule(final BaseApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return application;
    }

}
