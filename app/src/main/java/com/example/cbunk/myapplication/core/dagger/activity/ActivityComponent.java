package com.example.cbunk.myapplication.core.dagger.activity;

import com.example.cbunk.myapplication.core.dagger.application.ApplicationComponent;
import com.example.cbunk.myapplication.fyber.FyberActivity;
import com.example.cbunk.myapplication.fyber.model.FyberService;

import dagger.Component;

@ActivityInstanceScope
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, RetrofitModule.class})
public interface ActivityComponent {
    void inject(FyberActivity fyberActivity);

    FyberService fyberService();
}
