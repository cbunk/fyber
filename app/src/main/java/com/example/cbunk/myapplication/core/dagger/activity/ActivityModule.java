package com.example.cbunk.myapplication.core.dagger.activity;

import com.example.cbunk.myapplication.core.BaseActivity;
import com.example.cbunk.myapplication.core.BaseApplication;
import com.example.cbunk.myapplication.fyber.FyberContract;
import com.example.cbunk.myapplication.fyber.model.FyberModel;
import com.example.cbunk.myapplication.fyber.model.FyberService;

import android.app.ActionBar;
import android.content.Context;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {

    private final BaseActivity activity;

    public ActivityModule(final BaseActivity activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityInstanceScope
    public BaseApplication provideApplication() {
        return (BaseApplication) activity.getApplicationContext();
    }

    @Provides
    @ActivityInstanceScope
    public ActionBar provideActionBar() {
        return activity.getActionBar();
    }

    @Provides
    public FyberContract.Model provideFyberModel(FyberService fyberService) {
        return new FyberModel(fyberService);
    }

    @Provides
    public Context provideContext() {
        return activity;
    }

}
