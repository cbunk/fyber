package com.example.cbunk.myapplication.core.retrofit;

import java.io.File;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.cbunk.myapplication.core.BaseApplication;

import javax.inject.Inject;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitConfiguration {

    public static final String FYBER_API_KEY = "1c915e3b5d42d05136185030892fbb846c278927";

    private static final String CACHE_FILE_NAME = "responses";
    private static final int CACHE_SIZE = 10 * 1024 * 1024;
    private static final String BASE_URL = "http://api.fyber.com/feed/v1/";

    public RetrofitConfiguration(SharedPreferences sharedPreferences) {
    }

    public static Retrofit retrofit(final Context context) {
        Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl(BASE_URL);

        builder.addConverterFactory(GsonConverterFactory.create());
        builder.addCallAdapterFactory(RxJavaCallAdapterFactory.create());
        builder.client(createOkHttpClient(context));

        return builder.build();
    }

    private static OkHttpClient createOkHttpClient(final Context context) {
        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();

        BaseApplication baseApplication = (BaseApplication) context.getApplicationContext();
        String apiKey = baseApplication.getApiKey();

        okHttpClientBuilder.interceptors().add(new RequestInterceptor(apiKey));
        okHttpClientBuilder.interceptors().add(new ResponseInterceptor(apiKey));
//        client.networkInterceptors().add(new RewriteCacheControlInterceptor());

        // setup cache
        File httpCacheDirectory = new File(context.getCacheDir(), CACHE_FILE_NAME);
        int cacheSize = 10 * 1024 * 1024; // 10 MiB
        Cache cache = new Cache(httpCacheDirectory, cacheSize);

        // add cache to the client
        okHttpClientBuilder.cache(cache);
        return okHttpClientBuilder.build();
    }
}
