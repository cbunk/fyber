package com.example.cbunk.myapplication.core.dagger.activity;

import javax.inject.Singleton;

import com.example.cbunk.myapplication.core.retrofit.RetrofitConfiguration;
import com.example.cbunk.myapplication.fyber.model.FyberService;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class RetrofitModule {

    @Provides
    @ActivityInstanceScope
    public Retrofit provideRetrofit(final Context context) {
        return RetrofitConfiguration.retrofit(context);
    }

    @Provides
    @ActivityInstanceScope
    public FyberService provideFyberService(final Retrofit retrofit) {
        return retrofit.create(FyberService.class);
    }
}
