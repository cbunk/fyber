package com.example.cbunk.myapplication.fyber;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.cbunk.myapplication.R;
import com.example.cbunk.myapplication.core.BaseActivity;
import com.example.cbunk.myapplication.core.BaseApplication;
import com.example.cbunk.myapplication.core.dagger.activity.ActivityComponent;
import com.example.cbunk.myapplication.core.view.EndlessRecyclerOnScrollListener;
import com.example.cbunk.myapplication.fyber.FyberContract.View;
import com.example.cbunk.myapplication.fyber.model.pojo.Offer;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;

/**
 * Created by christian on 20.03.16.
 */
public class FyberActivity extends BaseActivity implements View {

    public static final String UID = "uid";
    public static final String API_KEY = "apiKey";
    public static final String APP_ID = "appId";
    public static final String PUB_0 = "pub0";
    @Inject
    FyberPresenter fyberPresenter;

    @Bind(R.id.fyber_activity_recycler_view)
    RecyclerView recyclerView;
    private OfferAdapter offerAdapter;
    private String uid;
    private String apiKey;
    private String appId;
    private String pub0;

    public static Intent create(Context context, String uid, String apiKey, String appId, String pub0) {
        Intent intent = new Intent(context, FyberActivity.class);

        BaseApplication baseApplication = (BaseApplication) context.getApplicationContext();
        baseApplication.setApiKey(apiKey);

        intent.putExtra(UID, uid);
        intent.putExtra(API_KEY, apiKey);
        intent.putExtra(APP_ID, appId);
        intent.putExtra(PUB_0, pub0);

        return intent;
    }

    @Override
    protected int layoutToInflate() {
        return R.layout.fyber_activity;
    }

    @Override
    protected void doInjection(ActivityComponent component) {
        component.inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fyberPresenter.setView(this);

        offerAdapter = new OfferAdapter();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(offerAdapter);

        uid = getIntent().getStringExtra(UID);
        apiKey = getIntent().getStringExtra(API_KEY);
        appId = getIntent().getStringExtra(APP_ID);
        pub0 = getIntent().getStringExtra(PUB_0);

        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(final int nextPage) {
                fyberPresenter.getOffers(nextPage, uid, apiKey, appId, pub0);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        fyberPresenter.getOffers(1, uid, apiKey, appId, pub0);
    }

    @Override
    public void showOffers(List<Offer> offers) {
        offerAdapter.add(offers);
    }

    @Override
    public void showErrorMessage(String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_SHORT).show();
    }
}
