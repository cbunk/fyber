package com.example.cbunk.myapplication.core;

import com.example.cbunk.myapplication.core.dagger.application.ApplicationComponent;
import com.example.cbunk.myapplication.core.dagger.application.ApplicationModule;
import com.example.cbunk.myapplication.core.dagger.application.DaggerApplicationComponent;
import com.example.cbunk.myapplication.core.dagger.activity.RetrofitModule;

import android.app.Application;

public class BaseApplication extends Application {

    private ApplicationComponent component;
    private String apiKey;

    @Override
    public void onCreate() {
        super.onCreate();

        initDependencyInjection();

    }

    public ApplicationComponent getComponent() {
        return component;
    }

    private void initDependencyInjection() {
        DaggerApplicationComponent.Builder builder = DaggerApplicationComponent.builder()
                                                                               .applicationModule(new ApplicationModule(
                    this));

        component = builder.build();
        component.inject(this);
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
}
