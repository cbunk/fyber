package com.example.cbunk.myapplication.fyber;

import com.example.cbunk.myapplication.core.rxjava.ObservableHelper;
import com.example.cbunk.myapplication.fyber.model.pojo.Fyber;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by christian on 20.03.16.
 */
public class FyberPresenter implements FyberContract.Presenter {
    public static final String JSON = "json";
    public static final String DE = "DE";
    public static final String OFFER_TYPES = "112";
    private final FyberContract.Model model;

    private FyberContract.View view;

    @Inject
    public FyberPresenter(FyberContract.Model model) {
        this.model = model;
    }

    public void setView(FyberContract.View view) {
        this.view = view;
    }

    @Override
    public void getOffers(final int page, String uId, String apiKey, String appId, String pub0) {
        Observable<Fyber> fyberObservable = model.getOffers(page, JSON, appId , uId, DE, OFFER_TYPES, pub0);

        ObservableHelper.prepare(fyberObservable).subscribe(new Subscriber<Fyber>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                view.showErrorMessage(e.getMessage());
            }

            @Override
            public void onNext(Fyber fyber) {
                if (fyber.getOffers().isEmpty()) {
                    if (page == 1) {
                        view.showErrorMessage("No offers");
                    } else {
                        view.showErrorMessage("No more offers");
                    }
                }


                view.showOffers(fyber.getOffers());
            }
        });
    }
}
