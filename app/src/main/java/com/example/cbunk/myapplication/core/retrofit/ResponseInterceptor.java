package com.example.cbunk.myapplication.core.retrofit;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by christian on 20.03.16.
 */
public class ResponseInterceptor implements Interceptor {
    private String apiKey;

    public ResponseInterceptor(String apiKey) {
        this.apiKey = apiKey;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Response response = chain.proceed(chain.request());

        String header = response.header("X-Sponsorpay-Response-Signature");

        String responseBodyString = response.body().string();
        String sha1 = new String(Hex.encodeHex(
                DigestUtils.sha1(responseBodyString + apiKey)));


        Response.Builder responseBuilder = response.newBuilder();

        if (response.isSuccessful() && !sha1.equals(header)) {
            responseBuilder.code(500); // set the status code to 500 so we'll get an error in the app
        }

        return responseBuilder
                .body(ResponseBody.create(response.body().contentType(), responseBodyString))
                .build();
    }
}
