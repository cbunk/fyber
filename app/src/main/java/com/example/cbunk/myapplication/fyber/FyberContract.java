package com.example.cbunk.myapplication.fyber;

import com.example.cbunk.myapplication.fyber.model.pojo.Fyber;
import com.example.cbunk.myapplication.fyber.model.pojo.Offer;

import java.util.List;

import rx.Observable;

/**
 * Created by christian on 20.03.16.
 */
public interface FyberContract {
    interface Model {
        Observable<Fyber> getOffers(int page, String format, String appId, String uId,
                                    String locale, String offerTypes, String pub0);
    }

    interface View {
        void showOffers(List<Offer> offers);

        void showErrorMessage(String message);
    }

    interface Presenter {
        void getOffers(int page, String uId, String apiKey, String appId, String pub0);
    }
}
