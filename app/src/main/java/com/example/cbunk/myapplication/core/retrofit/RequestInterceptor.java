package com.example.cbunk.myapplication.core.retrofit;

import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by cbunk on 14.01.16.
 */
public class RequestInterceptor implements Interceptor {

    private String apiKey;

    public RequestInterceptor(String apiKey) {
        this.apiKey = apiKey;
    }

    @Override
    public Response intercept(final Chain chain) throws IOException {
        Request request = chain.request();

        HttpUrl httpUrl = request.url();

        String encodedQuery = httpUrl.encodedQuery();
        encodedQuery = encodedQuery + "&" + apiKey;


        String sha1 = "";
//        new String(Hex.encodeHex(cript.digest()),
//                CharSet.forName("UTF-8"));

        sha1 = new String(Hex.encodeHex(DigestUtils.sha1(encodedQuery)));



        httpUrl = httpUrl.newBuilder().addQueryParameter("hashkey", sha1).build();

        return chain.proceed(request.newBuilder().url(httpUrl).build());
    }
}
