package com.example.cbunk.myapplication.core;

import javax.inject.Inject;

import com.example.cbunk.myapplication.R;
import com.example.cbunk.myapplication.core.dagger.activity.ActivityComponent;
import com.example.cbunk.myapplication.core.dagger.activity.ActivityModule;
import com.example.cbunk.myapplication.core.dagger.activity.DaggerActivityComponent;
import com.example.cbunk.myapplication.core.dagger.activity.RetrofitModule;
import com.example.cbunk.myapplication.core.dagger.application.ApplicationComponent;
import com.example.cbunk.myapplication.core.espresso.EspressoIdlingResource;

import android.os.Bundle;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

import android.support.test.espresso.IdlingResource;

import android.support.v4.content.ContextCompat;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {

    @Nullable
    protected Toolbar toolbar;

    private ActivityComponent activityComponent;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layoutToInflate());
        doInjection(createComponent());

        // do the view injection
        ButterKnife.bind(this);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.toolbar)); // generic rgb
        }
    }

    protected void doInjection(final ActivityComponent component) { }

    private ActivityComponent createComponent() {
        ApplicationComponent applicationComponent = ((BaseApplication) getApplication()).getComponent();
        activityComponent =
            DaggerActivityComponent.builder()
                    .applicationComponent(applicationComponent)
                    .activityModule(new ActivityModule(this))
                    .retrofitModule(new RetrofitModule()).build();
        return activityComponent;
    }

    @LayoutRes
    protected abstract int layoutToInflate();

    @Nullable
    protected View getView() {
        return findViewById(android.R.id.content);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home :
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @VisibleForTesting
    public IdlingResource getCountingIdlingResource() {
        return EspressoIdlingResource.getIdlingResource();
    }

    public ActivityComponent getActivityComponent() {
        return activityComponent;
    }
}
