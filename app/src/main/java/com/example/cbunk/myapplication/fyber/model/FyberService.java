package com.example.cbunk.myapplication.fyber.model;

import com.example.cbunk.myapplication.fyber.model.pojo.Fyber;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by christian on 20.03.16.
 */
public interface FyberService {

    @GET("offers.json/")
    Observable<Fyber> getOffers(@Query("appid") String appid,
                                @Query("format") String format,
                                @Query("locale") String locale,
                                @Query("offer_types") String offerTypes,
                                @Query("page") int page,
                                @Query("pub0") String pub0,
                                @Query("timestamp") long timestamp,
                                @Query("uid") String uid);
}
