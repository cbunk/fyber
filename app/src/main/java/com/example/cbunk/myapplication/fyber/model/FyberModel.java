package com.example.cbunk.myapplication.fyber.model;

import com.example.cbunk.myapplication.fyber.FyberContract;
import com.example.cbunk.myapplication.fyber.model.pojo.Fyber;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by christian on 20.03.16.
 */
public class FyberModel implements FyberContract.Model {

    private FyberService fyberService;

    @Inject
    public FyberModel(FyberService fyberService) {
        this.fyberService = fyberService;
    }

    @Override
    public Observable<Fyber> getOffers(int page, String format, String appId, String uId, String locale, String offerTypes, String pub0) {
        if (format == null) {
            return Observable.error(new Exception("format must not be null"));
        }

        if (format.isEmpty()) {
            return Observable.error(new Exception("format must not be empty"));
        }

        if (page < 1) {
            return Observable.error(new Exception("page must not be less than 1"));
        }

        if (appId == null) {
            return Observable.error(new Exception("appId must not be null"));
        }

        if (appId.isEmpty()) {
            return Observable.error(new Exception("appId must not be empty"));
        }

        if (uId == null) {
            return Observable.error(new Exception("uId must not be null"));
        }

        if (uId.isEmpty()) {
            return Observable.error(new Exception("uId must not be empty"));
        }

        if (locale == null) {
            return Observable.error(new Exception("locale must not be null"));
        }

        if (locale.isEmpty()) {
            return Observable.error(new Exception("locale must not be empty"));
        }

        return fyberService.getOffers(appId, format, locale, offerTypes, page, pub0, System.currentTimeMillis()/1000, uId);
    }
}
