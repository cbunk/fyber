package com.example.cbunk.myapplication.fyber;

import android.widget.Button;
import android.widget.EditText;

import com.example.cbunk.myapplication.R;
import com.example.cbunk.myapplication.core.BaseActivity;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by christian on 20.03.16.
 */
public class FyberFormActivity extends BaseActivity {

    @Bind(R.id.fyber_form_apikey_edittext)
    EditText apiKeyEditText;

    @Bind(R.id.fyber_form_appid_edittext)
    EditText appIdEditText;

    @Bind(R.id.fyber_form_pub0_edittext)
    EditText pub0EditText;

    @Bind(R.id.fyber_form_uid_edittext)
    EditText uidEditText;

    @Bind(R.id.fyber_form_submit_button)
    Button submitButton;

    @Override
    protected int layoutToInflate() {
        return R.layout.fyber_form_activity;
    }

    @OnClick(R.id.fyber_form_submit_button)
    public void onClickSubmit() {
        String uid = uidEditText.getText().toString();
        String apiKey = apiKeyEditText.getText().toString();
        String appId = appIdEditText.getText().toString();
        String pub0 = pub0EditText.getText().toString();

        boolean hasError = false;
        if (apiKey.isEmpty()) {
            apiKeyEditText.setError("ApiKey must not be empty");
            hasError = true;
        }

        if (uid.isEmpty()) {
            uidEditText.setError("uid must not be empty");
            hasError = true;
        }

        if (appId.isEmpty()) {
            appIdEditText.setError("appId must not be empty");
            hasError = true;
        }

        if (hasError) {
            return;
        }

        startActivity(FyberActivity.create(this, uid, apiKey, appId, pub0));
    }

}
