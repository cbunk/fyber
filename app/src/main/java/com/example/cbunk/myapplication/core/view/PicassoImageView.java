package com.example.cbunk.myapplication.core.view;

import com.example.cbunk.myapplication.R;

import com.google.common.base.Strings;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import android.content.Context;

import android.util.AttributeSet;

import android.widget.ImageView;

/**
 * Created by christian on 04.08.14.
 */
public class PicassoImageView extends ImageView {

    // Available Image Scaling types : supported by Picasso library
    public enum ScaleType {
        FIT,
        CENTER_CROP, // Maintains Image Aspect Ratio
        CENTER_INSIDE
    }

    private Context context;

    // For caching image loading
    private final boolean cacheRequests = true;

    // For Image Scaling type
    private ScaleType scaleType = ScaleType.FIT;

    // Variables for image placeholders while executing image loading request
    private final boolean showPlaceholder = true;
    private final int loadingDrawable = R.drawable.icon_loading_placeholder;
    private final int failureDrawable = R.drawable.icon_loading_failure;

    // Parameters used in case of scaleType is CENTER_CROP | CENTER_INSIDE
    private int targetWidth = -1;
    private int targetHeight = -1;

    // For Debugging
    private final boolean showIndicators = true;

    public PicassoImageView(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public PicassoImageView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public PicassoImageView(final Context context) {
        super(context);
        init(context);
    }

    private void init(final Context context) {
        this.context = context;
    }

    public void setScaleType(final ScaleType scaleType) {
        this.scaleType = scaleType;
    }

    /**
     * Resize the image to the specified size in pixels.
     */
    public void resize(final int targetWidth, final int targetHeight) {
        this.targetWidth = targetWidth;
        this.targetHeight = targetHeight;
    }

    /**
     * Set the image URI and load it.
     */
    public void setImageUrl(final String uri) {
        if (Strings.isNullOrEmpty(uri)) {
            return;
        }

        Picasso picasso = Picasso.with(this.context);
        picasso.setIndicatorsEnabled(showIndicators);

        RequestCreator requestCreator = picasso.load(uri);

        if (showPlaceholder) {
            requestCreator = requestCreator.placeholder(this.loadingDrawable).error(this.failureDrawable);
        }

        switch (scaleType) {

            case FIT :
                requestCreator = requestCreator.fit();
                break;

            case CENTER_CROP :

                requestCreator = checkResizeParams(requestCreator);
                requestCreator = requestCreator.centerCrop();
                break;

            case CENTER_INSIDE :
                requestCreator = checkResizeParams(requestCreator);
                requestCreator = requestCreator.centerInside();
                break;
        }

        requestCreator = requestCreator.skipMemoryCache();
        requestCreator.into(this);
    }

    private RequestCreator checkResizeParams(final RequestCreator requestCreator) {

        if (-1 == this.targetWidth || -1 == this.targetHeight) {
            throw new IllegalStateException("Must call resize(width, height) function before");
        }

        return requestCreator.resize(this.targetWidth, this.targetHeight);
    }
}
