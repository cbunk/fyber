package com.example.cbunk.myapplication.fyber;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.cbunk.myapplication.R;
import com.example.cbunk.myapplication.core.BaseViewHolder;
import com.example.cbunk.myapplication.core.view.BaseTextView;
import com.example.cbunk.myapplication.core.view.PicassoImageView;
import com.example.cbunk.myapplication.fyber.model.pojo.Fyber;
import com.example.cbunk.myapplication.fyber.model.pojo.Offer;

import butterknife.Bind;

/**
 * Created by christian on 20.03.16.
 */
public class OfferViewHolder extends BaseViewHolder<Offer> {

    @Bind(R.id.offer_item_title_textview)
    BaseTextView titleTextView;

    @Bind(R.id.offer_item_teaser_textview)
    BaseTextView teaserTextView;

    @Bind(R.id.offer_item_thumbnail_imageview)
    PicassoImageView picassoImageView;

    @Bind(R.id.offer_item_payout_textview)
    TextView payoutTextView;

    public static OfferViewHolder create(ViewGroup parent) {
        return new OfferViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.offer_item, parent, false));

    }

    private OfferViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    protected void bindData(Offer offer, int position) {
        titleTextView.setText(offer.getTitle());
        teaserTextView.setText(offer.getTeaser());
        picassoImageView.setImageUrl(offer.getThumbnail().getHires());
        payoutTextView.setText("Payout: "+String.valueOf(offer.getPayout()));
    }
}
