package com.example.cbunk.myapplication.core.rxjava;

import rx.Observable;

import rx.android.schedulers.AndroidSchedulers;

import rx.schedulers.Schedulers;

public class ObservableHelper {

    /**
     * Subscribe in new thread. Observe on main thread.
     */
    public static <T> Observable<T> prepare(final Observable<T> observable) {
        return observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }
}
