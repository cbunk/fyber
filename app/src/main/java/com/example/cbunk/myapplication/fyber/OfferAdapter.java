package com.example.cbunk.myapplication.fyber;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.example.cbunk.myapplication.core.BaseAdapter;
import com.example.cbunk.myapplication.fyber.model.pojo.Fyber;
import com.example.cbunk.myapplication.fyber.model.pojo.Offer;

/**
 * Created by christian on 20.03.16.
 */
public class OfferAdapter extends BaseAdapter<Offer> {

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position, Offer offer) {
        if (holder instanceof OfferViewHolder) {
            OfferViewHolder offerViewHolder = (OfferViewHolder) holder;
            offerViewHolder.bindData(offer, position);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return OfferViewHolder.create(parent);
    }
}
